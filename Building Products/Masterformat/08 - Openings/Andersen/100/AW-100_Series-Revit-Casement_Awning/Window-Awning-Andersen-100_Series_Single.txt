,Width##LENGTH##INCHES,Height##LENGTH##INCHES,Designation##OTHER##,Type Comments##OTHER##,. . Lights Wide##OTHER##,. . Lights High##OTHER##,Fractional Lights Wide##OTHER##,Fractional Lights High##OTHER##
"17-1/2"" x 17-1/2""",17.5,17.5,100AS1616,Fibrex awning window,2,2,2,2
"17-1/2"" x 23-1/2""",17.5,23.5,100AS1620,Fibrex awning window,2,2,2,2
"17-1/2"" x 29-1/2""",17.5,29.5,100AS1626,Fibrex awning window,2,2,2,2
"17-1/2"" x 35-1/2""",17.5,35.5,100AS1630,Fibrex awning window,2,3,2,3
"23-1/2"" x 17-1/2""",23.5,17.5,100AS2016,Fibrex awning window,2,2,2,2
"23-1/2"" x 23-1/2""",23.5,23.5,100AS2020,Fibrex awning window,2,2,2,2
"23-1/2"" x 29-1/2""",23.5,29.5,100AS2026,Fibrex awning window,2,2,2,2
"23-1/2"" x 35-1/2""",23.5,35.5,100AS2030,Fibrex awning window,2,3,2,3
"29-1/2"" x 17-1/2""",29.5,17.5,100AS2616,Fibrex awning window,3,2,3,2
"29-1/2"" x 23-1/2""",29.5,23.5,100AS2620,Fibrex awning window,3,2,3,2
"29-1/2"" x 29-1/2""",29.5,29.5,100AS2626,Fibrex awning window,3,2,3,2
"29-1/2"" x 35-1/2""",29.5,35.5,100AS2630,Fibrex awning window,3,3,3,3
"35-1/2"" x 17-1/2""",35.5,17.5,100AS3016,Fibrex awning window,3,2,3,2
"35-1/2"" x 23-1/2""",35.5,23.5,100AS3020,Fibrex awning window,3,2,3,2
"35-1/2"" x 29-1/2""",35.5,29.5,100AS3026,Fibrex awning window,3,2,3,2
"35-1/2"" x 35-1/2""",35.5,35.5,100AS3030,Fibrex awning window,3,3,3,3
"41-1/2"" x 17-1/2""",41.5,17.5,100AS3616,Fibrex awning window,4,2,4,2
"41-1/2"" x 23-1/2""",41.5,23.5,100AS3620,Fibrex awning window,4,2,4,2
"41-1/2"" x 29-1/2""",41.5,29.5,100AS3626,Fibrex awning window,4,2,4,2
"41-1/2"" x 35-1/2""",41.5,35.5,100AS3630,Fibrex awning window,4,3,4,3
"47-1/2"" x 17-1/2""",47.5,17.5,100AS4016,Fibrex awning window,4,2,4,2
"47-1/2"" x 23-1/2""",47.5,23.5,100AS4020,Fibrex awning window,4,2,4,2
"47-1/2"" x 29-1/2""",47.5,29.5,100AS4026,Fibrex awning window,4,2,4,2
"47-1/2"" x 35-1/2""",47.5,35.5,100AS4030,Fibrex awning window,4,3,4,3

